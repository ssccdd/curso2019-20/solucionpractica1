[![logo](https://www.gnu.org/graphics/gplv3-127x51.png)](https://choosealicense.com/licenses/gpl-3.0/)
## PRIMERA PRACTICA
**RESOLVER CON SEMÁFOROS**

La práctica se divide en dos partes diferenciadas:

 1. **Resolución teórica:** Consiste en realizar el análisis y diseño del problema. Para ello hay que especificar los TDAs (Clases) que sean necesarias, con sus atributos (estado) y sus métodos (comportamiento). Hay que justificar las decisiones que se tomen sobre todo por qué se da una determinada responsabilidad a una determinada clase.
A la finalización de esta parte se debe entregar un archivo pdf en el nombre: ApellidosNombrePrac1.pdf, a través del sistema antiplagio que hay en ILIAS. Un porcentaje de plagio mayor al 10% tendrá como consecuencia la no evaluación de la práctica.
 3. **Implementación:** Para la implementación hay que utilizar semáforos [`Semaphore`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/Semaphore.html) y el marco de ejecución [`Executor`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/Executor.html) con alguna de las clases que implementan esas interfaces.
A la finalización de la práctica se subirá el proyecto NetBeans completo en formato zip en el nombre ApellidosNombrePrac1.zip. El código se someterá también a la detección de copias y un porcentaje mayor del 10% tendrá como consecuencia la no evaluación de la práctica.

**PROBLEMA A RESOLVER**

En una granja de rendering (render farm) se generan **Escenas** compuestas por **Fotogramas** que luego se renderizan. Todo el proceso se realiza concurrentemente. Las características del proceso son:

- Los **Fotogramas** tienen:
	- Identificador único para cada fotograma.
	- Un tiempo de cálculo aleatorio entre **MIN_TIEMPO_FOTOGRAMA** y **MIN_TIEMPO_FOTOGRAMA** + **VARIACION_TIEMPO** segundos. Este tiempo se calcula cuando se construye el fotograma y se conserva durante el resto de la vida del fotograma. Es el tiempo que tardará un renderizador de escenas en renderizar este fotograma.

- Las **Escenas** tienen:
	- Identificador único para cada escena.
	- Prioridad **ALTA** o **BAJA** que indica si se atenderán antes o después por los renderizadores de escenas. Sólo cuando no hay ninguna escena prioritaria a la espera de ser renderizada se atienden las escenas no prioritarias.
	- Conjunto de fotogramas con tamaño aleatorio entre **MIN_NUM_FOTOGRAMAS** y **MIN_NUM_FOTOGRAMAS** + **VARIACION_NUM_FOTOGRAMAS**.
	- El tiempo que se tarda en renderizar una escena es la suma del tiempo de cada fotograma mas un tiempo fijo de: **TIEMPO_FINALIZACION_ESCENA**.

- Hay **Generadores de escenas** que recopilan todo lo necesario para renderizar una escena, la construyen y la ponen a disposición de los renderizadores. Un generador tarda un tiempo aleatorio entre **MIN_TIEMPO_GENERACION** y **MIN_TIEMPO_GENERACION** + **VARIACION_TIEMPO** segundos en generar la escena.

- También hay **Renderizadores de escenas** cuyo funcionamiento es:
	- Tomar una escena de las que hay disponibles. Siempre se elige la que tiene mas prioridad. Sólo cuando no hay escenas con prioridad **ALTA** se elige una escena con prioridad **BAJA**.
	- Renderizarla. Una **Escena** no está completa hasta que no se han completado todos sus **Fotogramas**.
	- Cuando una escena ha sido renderizada se coloca en una lista de escenas resultado.

- Los Generadores de escenas se comunican con los Renderizadores de escenas mediante dos listas de escenas comunes (Tanto los Generadores como los Renderizadores tienen acceso a esas dos listas):
	- Una lista es para las escenas prioritarias y la otra para las no prioritarias.
	- Los generadores escriben **Escenas** en las listas y los **Renderizadores** leen las escenas de esas listas.
	- Esas listas tienen un tamaño máximo de **MAX_ESCENAS_EN_ESPERA**. Si ya hay **MAX_ESCENAS_EN_ESPERA** en una lista y un generador tiene que escribir en ella, deberá esperar a que se saque alguna Escena antes de poder seguir escribiendo nuevas escenas en esa lista.

- Proceso general:
	- Se construirán **NUM_GENERADORES** Generadores de escenas.
	- Se construirán **NUM_RENDERIZADORES** Renderizadores de escenas.
	- Cada Generador de escenas construirá un número de escenas comprendido entre **MIN_NUM_ESCENAS** y **MIN_NUM_ESCENAS** + **VARIACION_NUM_ESCENAS**. (Cada escena tiene un número aleatorio de fotogramas tal y como se describe mas arriba).
	- Cuando los generadores han generado todas sus escenas, terminan su ejecución.
	- Cuando no quedan escenas en ninguna de las listas y los generadores han terminado su ejecución, los renderizadores terminan su ejecución.
	- Al finalizar la ejecución de todas las tareas, el programa mostrará:
		- El número total de escenas procesadas.
		- El tiempo total empleado en **Renderizar** todas las escenas.
		- Para cada escena:
			- La hora en que se generó.
			- La hora en que un Renderizador comenzó su procesamiento.
			- Los fotogramas que la componen y la duración de cada uno.
			- La hora en la que el Renderizador terminó su procesamiento.
			- El tiempo total empleado en el procesamiento de la escena.

# Análisis y Diseño

Estructuras de datos, variables compartidas y procedimientos necesarios[^1].
[^1]:Esto es pseudo código. Hay detalles que se han omitido. Por ejemplo, las variables compartidas en la implementación hay que pasarlas como parámetros. Aquí no se incluyen porque lo que interesa es analizar y diseñar el comportamiento de las funciones principales. Tampoco se incluyen los setters y getters que no sean importantes. Aunque sí hay que implementarlos.

## Variables compartidas

- `mainMutex: Mutex`
	Mutex para impedir que dos `RenderizadorEscena` comprueben a la vez si las listas de escenas pendientes están vacías.
- `barrera: Barrera`
	Barrera que permite saber cuando han terminado **todos** los generadores de escenas de generar todas sus escenas asignadas.
- `listaAltaPrioridad: Lista<Escena>`
	Lista de escenas de prioridad **ALTA** pendientes de procesar. Es compartida por los `GeneradorEscena` y los `RenderizadorEscena`.
- `emptySemAltaPrioridad: Semaforo`
	Semáforo para controlar que no se saque ninguna escena de la lista de escenas de prioridad **ALTA** si está vacía.
- `fillSemAltaPrioridad: Semáforo`
	Semáforo para controla que no se introduzca ninguna escena en la lista de escenas de prioridad **ALTA** si la lista ya tiene **MAX_ESCENAS_EN_ESPERA.**
- `mutexAltaPrioridad: Mutex`
	Mutex para controlar que la escritura/lectura en la lista de escenas de prioridad **ALTA** se haga en exclusión mútua.
		
- `listaBajaPrioridad: Lista<Escena>`
	Lista de escenas de prioridad **BAJA** pendientes de procesar. Es compartida por los `GeneradorEscena` y los `RenderizadorEscena`.
- `emptySemBajaPrioridad: Semaforo`
	Semáforo para controlar que no se saque ninguna escena de la lista de escenas de prioridad **BAJA** si está vacía.
- `fillSemBajaPrioridad: Semáforo`
	Semáforo para controla que no se introduzca ninguna escena en la lista de escenas de prioridad **BAJA** si la lista ya tiene **MAX_ESCENAS_EN_ESPERA.**
- `mutexBajaPrioridad: Mutex`
	Mutex para controlar que la escritura/lectura en la lista de escenas de prioridad **BAJA** se haga en exclusión mútua.

## Tipos de datos

### Fotograma

- Variables locales:

	- `id: CadenaCaracteres`
	Identificador que permite distinguir unos fotogramas de otros. Cada fotograma tiene su identificador, que es elegido secuencialmente con una única secuencia compartida por todos los fotogramas.
	- `duracion: Entero`
	Duración aleatoria en segundos de cada fotograma. Es un valor comprendido entre **MIN_TIEMPO_FOTOGRAMA** y **MIN_TIEMPO_FOTOGRAMA**+**VARIACION_TIEMPO**

- Funciones:
	```
	func crearFotograma
		id = nuevoIDFotogramaUnico()
		duracion = numeroAleatorioEntre(MIN_TIEMPO_FOTOGRAMA, MIN_TIEMPO_FOTOGRAMA+VARIACION_TIEMPO)
	Fin func
	```

### Escena

- Variables locales:

	- `id: CadenaCaracteres`
	Identificador que permite distinguir unas escenas de otras. Cada Escena tiene su identificador que es elegido secuencialmente con una única secuencia compartida por todas las escenas.
	- `prioridad: Prioridad`
	Prioridad de la escena. Toma los valores **ALTA** o **BAJA**.
	- `duracion: Entero`
	Tiempo que tarda esta escena en ser renderizada. Es la suma de la duración de cada fotograma mas un tiempo fijo **TIEMPO_FINALIZACION_ESCENA**.
	- `horaGeneracion: Hora`
	Hora en la que se generó la escena.
	- `horaInicioProcesamiento: Hora`
	Hora en la que se comenzó el procesamiento de la escena.
	- `horaFinProcesamiento: Hora`
	Hora en la que se terminó el procesamiento de la escena.  
	- `listaFotogramas: Lista<Fotograma>`
	Lista con los fotogramas que componen la escena.
	
- Funciones:

	```
	func construyeEscena
		id = nuevoIDEscenaUnico()
		prioridad = nuevaPrioridadAleatoria()
		duracion = TIEMPO_FINALIZACION_ESCENA
		listaFotogramas = nueva lista vacía de Fotograma
	Fin func
	```

	```
	func inicializarEscena
		horaGeneracion = HoraActual()
		numFotogramasAGenerar = numeroAleatorioEntre(MIN_NUM_FOTOGRAMAS, MIN_NUM_FOTOGRAMAS+VARIACION_NUM_FOTOGRAMAS)
		Mientras noSeHayanGenerado numFotogramasAGenerar
			fotograma = crearFotograma()
			listaFotogramas.mete(fotograma)
			duracion = duracion + fotograma.duracion
		Fin Mientras
	Fin func
	```
### GeneradorEscena

- Variables locales:

	- `id: CadenaCaracteres`
	Identificador que permite distinguir unos generadores de otros.
	
- Funciones:

	```
	func ejecucion -> Lista<Escena>
		resultado: Lista<Escena>
		numEscenasAGenerar = numeroAleatorioEntre(MIN_NUM_ESCENAS, MIN_NUM_ESCENAS+VARIACION_NUM_ESCENAS)
		Mientras noSeHayanGenerado numEscenasAGenerar
			tiempoGeneracionEscena = numeroAleatorioEntre(MIN_TIEMPO_GENERACION, MIN_TIEMPO_GENERACION+VARIACION_TIEMPO)
			esperar(tiempoGeneracionEscena)
			escena = construyeEscena()
			escena.inicializarEscena()
			resultado.mete(escena)
			Si escena.prioridad() == ALTA
				fillSemAltaPrioridad.wait()
				mutexAltaPrioridad.wait()
				listaAltaPrioridad.mete(escena)
				mutexAltaPrioridad.signal()
				emptySemAltaPrioridad.signal()
			Si no
				fillSemBajaPrioridad.wait()
				mutexBajaPrioridad.wait()
				listaBajaPrioridad.mete(escena)
				mutexBajaPrioridad.signal()
				emptySemBajaPrioridad.signal()
			Fin Si
		Fin Mientras 
		barrera.espera()
		devolver resultado
	Fin func
	```
### RenderizadorEscenas

- Variables locales:
	- `id: CadenaCaracteres`
	Identificador que permite distinguir unos renderizadores de otros.
	- `terminadosGeneradores: Booleano`
	Bandera booleana que permite notificar a los renderizadores que todos los generadores han terminado.
	
- Funciones:

	```
	func ejecucion -> Lista<Escena>
		escena: Escena
        tengoEscena: Booleano
        resultado: Lista<Escena>
        Mientras verdadero
			tengoEscena = falso
			mainMutex.wait()
			Si listaAltaPrioridad.elementos != 0
				emptySemAltaPrioridad.wait()
				mutexAltaPrioridad.wait()
				escena = listaAltaPrioridad.saca()
				mutexAltaPrioridad.signal()
				fillSemAltaPrioridad.signal()
				tengoEscena = verdadero
			Si no Si listaBajaPrioridad.elementos != 0
				emptySemBajaPrioridad.wait()
				mutexBajaPrioridad.wait()
				escena = listaBajaPrioridad.saca()
				mutexBajaPrioridad.signal()
				fillSemBajtaPrioridad.signal()
				tengoEscena = verdadero
			Si no Si terminadosGeneradores
				mainMutex.signal()
				devolver resultado
			Fin Si
			mainMutex.signal()
			Si tengoEscena
				escena.horaInicioProcesamiento = horaActual()
				EsperarTiempo(escena.duracion)
				escena.horaFinProcesamiento = horaActual()
				resultado.mete(escena)
			Si no
				EsperarTiempo(RETARDO)
			Fin Si
		Fin Mientras
	Fin func
	```
	
	```
	func setTerminadosGeneradores()
		terminadosGeneradores = true
	Fin func
	```

### Finalizador

- Variables locales:

	- `listaRenderizadores: Lista<RenderizadorEscenas>`
	La lista de los renderizadores actualmente en ejecución para poder avisarles de que los generadores han terminado su ejecución.

- Funciones:

	```
	func ejecucion
		ParaCada renderizador en listaRenderizadores
			renderizador.setTerminadosGeneradores()
		Fin ParaCada
	Fin func
	```

### Proceso Sistema (Hilo Principal)

- Funciones:

	```
	func principal
		inicializarVariablesCompartidas()
		crearGeneradores(NUM_GENERADORES) 		
		crearRenderizadores(NUM_RENDERIZADORES)
		ejecutarTodasLasTareas()  
		EsperarFinalizacionTodasLasTareas()
		Mostrar númeroTotalEscenasGeneradas
        Mostrar tiempoTotalRenderizaciónRscenas
        ParaCada escena 
			Mostrar escena.horaGeneración  
			Mostrar escena.horaInicioProcesamiento 
			Mostrar escena.horaFinProcesamiento 
			Mostrar escena.duracion
			Mostrar escena.fotogramas  
	    Fin ParaCada
	Fin func
	```

	```
	func inicializarVariablesCompartidas
		// - Solo se incluyen las que no se inicializan con valores por defecto.
		emptySemAltaPrioridad <- 0
		fillSemAltaPrioridad <- MAX_ESCENAS_EN_ESPERA
		emptySemBajaPrioridad <- 0
		fillSemBajaPrioridad <- MAX_ESCENAS_EN_ESPERA
	Fin func
	```
