/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uja.ssccdd.curso1920.solucionpractica1;

import static com.uja.ssccdd.curso1920.solucionpractica1.Constantes.MAX_ESCENAS_EN_ESPERA;
import static com.uja.ssccdd.curso1920.solucionpractica1.Constantes.NUM_GENERADORES;
import static com.uja.ssccdd.curso1920.solucionpractica1.Constantes.NUM_RENDERIZADORES;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fconde
 */
public class SolucionPractica1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        ReentrantLock mainMutex = new ReentrantLock();
        
        ArrayList<Escena> listaAltaPrioridad = new ArrayList<Escena>();
        Semaphore emptySemAltaPrioridad = new Semaphore(0);
        Semaphore fillSemAltaPrioridad = new Semaphore(MAX_ESCENAS_EN_ESPERA);
        ReentrantLock mutexAltaPrioridad = new ReentrantLock();
        ArrayList<Escena> listaBajaPrioridad = new ArrayList<Escena>();
        Semaphore emptySemBajaPrioridad = new Semaphore(0);
        Semaphore fillSemBajaPrioridad = new Semaphore(MAX_ESCENAS_EN_ESPERA);
        ReentrantLock mutexBajaPrioridad = new ReentrantLock();
        
        System.out.println("** Hilo(PRINCIPAL): Ha iniciado la ejecucion\n");

        Finalizador finalizador = new Finalizador();
        CyclicBarrier barrier = new CyclicBarrier(NUM_GENERADORES, finalizador);
        
        ExecutorService executor = Executors.newFixedThreadPool(NUM_GENERADORES+NUM_RENDERIZADORES);
        ArrayList<Callable<List<Escena>>> tareas = new ArrayList<Callable<List<Escena>>>();
        for (int i=0; i<NUM_GENERADORES; i++) {
            GeneradorEscenas generador = new GeneradorEscenas("GEN-"+i, listaAltaPrioridad, 
                    emptySemAltaPrioridad, fillSemAltaPrioridad, mutexAltaPrioridad, 
                    listaBajaPrioridad, emptySemBajaPrioridad, fillSemBajaPrioridad, 
                    mutexBajaPrioridad, barrier);
            tareas.add(generador);
        }
        for (int i=0; i<NUM_RENDERIZADORES; i++) {
            RenderizadorEscenas renderizador = new RenderizadorEscenas("REND-"+i, 
                    mainMutex, listaAltaPrioridad,  emptySemAltaPrioridad, fillSemAltaPrioridad, 
                    mutexAltaPrioridad, listaBajaPrioridad, emptySemBajaPrioridad, 
                    fillSemBajaPrioridad, mutexBajaPrioridad);
            tareas.add(renderizador);
        }

        List<Future<List<Escena>>> resultados = null;
        try {
            resultados = executor.invokeAll(tareas);
        } catch (InterruptedException ex) {
            Logger.getLogger(SolucionPractica1.class.getName()).log(Level.SEVERE, null, ex);
        }

        // - Hacer que el marco de ejecucion no pueda recibir nuevas tareas
        executor.shutdown();

        System.out.println("\n** Hilo(PRINCIPAL): Todos los hilos han terminado\n");

        // - En este punto todos los hilos han terminado. Presetnar los resultados.
        System.out.println("** Hilo(PRINCIPAL): MOSTRANDO GENERADORES");
        for (int i=0; i<NUM_GENERADORES; i++) {
            Future<List<Escena>> resultado = resultados.get(i);
            List<Escena> res;
            try {
                res = resultado.get();
                System.out.println(res);
            } catch (InterruptedException ex) {
                Logger.getLogger(SolucionPractica1.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ExecutionException ex) {
                Logger.getLogger(SolucionPractica1.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        System.out.println("\n** Hilo(PRINCIPAL): MOSTRANDO RENDERIZADORES");
        for (int i=NUM_GENERADORES; i<NUM_GENERADORES+NUM_RENDERIZADORES; i++) {
            Future<List<Escena>> resultado = resultados.get(i);
            List<Escena> res;
            try {
                res = resultado.get();
                System.out.println(res);
            } catch (InterruptedException ex) {
                Logger.getLogger(SolucionPractica1.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ExecutionException ex) {
                Logger.getLogger(SolucionPractica1.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        System.out.println("** Hilo(PRINCIPAL): Ha finalizado la ejecucion");
    }
}
