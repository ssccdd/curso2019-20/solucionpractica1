/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uja.ssccdd.curso1920.solucionpractica1;

import static com.uja.ssccdd.curso1920.solucionpractica1.Constantes.MIN_NUM_FOTOGRAMAS;
import com.uja.ssccdd.curso1920.solucionpractica1.Constantes.Prioridad;
import static com.uja.ssccdd.curso1920.solucionpractica1.Constantes.TIEMPO_FINALIZACION_ESCENA;
import static com.uja.ssccdd.curso1920.solucionpractica1.Constantes.VARIACION_NUM_FOTOGRAMAS;
import static com.uja.ssccdd.curso1920.solucionpractica1.Constantes.aleatorio;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @author fconde
 */
public class Escena {
    
    private static ReentrantLock privateMutex = new ReentrantLock();
    private static int ultimoIDAsignado = 0;

    private String id;
    private Prioridad prioridad;
    private int duracion;
    private List<Fotograma> listaFotogramas;
    private Calendar horaGeneracion;
    private Calendar horaInicioProcesamiento;
    private Calendar horaFinProcesamiento;

    public Escena() {
        id = "ESC-" + Escena.nuevoIDEscenaUnico();
        prioridad = Prioridad.getPrioridadAleatoria();
        duracion = TIEMPO_FINALIZACION_ESCENA;
        listaFotogramas = new ArrayList<Fotograma>();
    }

    public void inicializarEscena() {
        horaGeneracion = Calendar.getInstance();
        int numFotogramasAGenerar = MIN_NUM_FOTOGRAMAS + aleatorio.nextInt(VARIACION_NUM_FOTOGRAMAS);
        for (int i = 0; i < numFotogramasAGenerar; i++) {
            Fotograma fotograma = new Fotograma();
            listaFotogramas.add(fotograma);
            duracion = duracion + fotograma.getDuracion();
        }
    }

    private String formatearTiempo(Calendar tiempo) {
        Date date = tiempo.getTime();
        DateFormat dateFormat = new SimpleDateFormat("hh:mm:ss");
        String strDate = dateFormat.format(date);
        return strDate;
    }

    @Override
    public String toString() {
        return "Escena{" + id + ", " + prioridad + ", dur=" + duracion
                + ", horaGen: " + formatearTiempo(horaGeneracion) + ", horaInicio: "
                + formatearTiempo(horaInicioProcesamiento) + ", horaFin: "
                + formatearTiempo(horaFinProcesamiento) + ")"
                + "\nFotogramas=" + listaFotogramas + "\n\n";
    }

    public void setHoraInicioProcesamiento() {
        this.horaInicioProcesamiento = Calendar.getInstance();
    }

    public void setHoraFinProcesamiento() {
        this.horaFinProcesamiento = Calendar.getInstance();
    }

    public String getId() {
        return id;
    }

    public Prioridad getPrioridad() {
        return prioridad;
    }

    public int getDuracion() {
        return duracion;
    }
    
    public String getHoraGeneracionStr() {
        return formatearTiempo(horaGeneracion);
    }
    
    public static int nuevoIDEscenaUnico() {
        int idUnico;
        privateMutex.lock();
        idUnico = ultimoIDAsignado;
        ultimoIDAsignado = ultimoIDAsignado + 1;
        privateMutex.unlock();
        return idUnico;
    }
}
