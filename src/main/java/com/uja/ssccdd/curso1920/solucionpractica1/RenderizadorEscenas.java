/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uja.ssccdd.curso1920.solucionpractica1;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fconde
 */
public class RenderizadorEscenas implements Callable<List<Escena>> {

    private static Boolean terminadosGeneradores = false;
    
    private String id;
    
    private ReentrantLock mainMutex;
    private ArrayList<Escena> listaAltaPrioridad;
    private Semaphore emptySemAltaPrioridad;
    private Semaphore fillSemAltaPrioridad;
    private ReentrantLock mutexAltaPrioridad;
    private ArrayList<Escena> listaBajaPrioridad;
    private Semaphore emptySemBajaPrioridad;
    private Semaphore fillSemBajaPrioridad;
    private ReentrantLock mutexBajaPrioridad;

    public RenderizadorEscenas(String id, ReentrantLock mainMutex, 
            ArrayList<Escena> listaAltaPrioridad,
            Semaphore emptySemAltaPrioridad, Semaphore fillSemAltaPrioridad, 
            ReentrantLock mutexAltaPrioridad, ArrayList<Escena> listaBajaPrioridad, 
            Semaphore emptySemBajaPrioridad, Semaphore fillSemBajaPrioridad, 
            ReentrantLock mutexBajaPrioridad) {
        
        this.id = id;
        this.mainMutex = mainMutex;
        this.listaAltaPrioridad = listaAltaPrioridad;
        this.emptySemAltaPrioridad = emptySemAltaPrioridad;
        this.fillSemAltaPrioridad = fillSemAltaPrioridad;
        this.mutexAltaPrioridad = mutexAltaPrioridad;
        this.listaBajaPrioridad = listaBajaPrioridad;
        this.emptySemBajaPrioridad = emptySemBajaPrioridad;
        this.fillSemBajaPrioridad = fillSemBajaPrioridad;
        this.mutexBajaPrioridad = mutexBajaPrioridad;
    }

    
    @Override
    public List<Escena> call() {
        System.out.println("   " + id + ": Ha iniciado la ejecucion\n");

        List<Escena> listaEscenasRenderizadas = new ArrayList<Escena>();
        Escena escena = null;
        Boolean tengoEscena;
        
        while(true) {
            tengoEscena = false;
            try {
                mainMutex.lock();
                if (listaAltaPrioridad.size() != 0) {
                    emptySemAltaPrioridad.acquire();
                    mutexAltaPrioridad.lock();
                    escena = listaAltaPrioridad.remove(0);
                    mutexAltaPrioridad.unlock();
                    fillSemAltaPrioridad.release();
                    tengoEscena = true;
                } else if (listaBajaPrioridad.size() != 0) {
                    emptySemBajaPrioridad.acquire();
                    mutexBajaPrioridad.lock();
                    escena = listaBajaPrioridad.remove(0);
                    mutexBajaPrioridad.unlock();
                    fillSemBajaPrioridad.release();
                    tengoEscena = true;
                } else if (terminadosGeneradores) {
                    mainMutex.unlock();
                    return listaEscenasRenderizadas;
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(RenderizadorEscenas.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            mainMutex.unlock();
            
            if (tengoEscena) {
                escena.setHoraInicioProcesamiento();
                try {
                    TimeUnit.SECONDS.sleep(escena.getDuracion());
                } catch (InterruptedException ex) {
                    Logger.getLogger(RenderizadorEscenas.class.getName()).log(Level.SEVERE, null, ex);
                }
                escena.setHoraFinProcesamiento();
                listaEscenasRenderizadas.add(escena);
                System.out.println("   " + id + ": Renderizando escena " + escena.getId() + 
                    "," + escena.getPrioridad());
            } else {
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException ex) {
                    Logger.getLogger(RenderizadorEscenas.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public static void setTerminadosGeneradores(Boolean terminadosGeneradores) {
        RenderizadorEscenas.terminadosGeneradores = terminadosGeneradores;
    }
}
