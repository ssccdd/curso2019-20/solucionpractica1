/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uja.ssccdd.curso1920.solucionpractica1;

import static com.uja.ssccdd.curso1920.solucionpractica1.Constantes.MIN_TIEMPO_FOTOGRAMA;
import static com.uja.ssccdd.curso1920.solucionpractica1.Constantes.VARIACION_TIEMPO;
import static com.uja.ssccdd.curso1920.solucionpractica1.Constantes.aleatorio;

/**
 *
 * @author fconde
 */
public class Fotograma {
    
    private String id;
    private int duracion;

    public Fotograma() {
        duracion = MIN_TIEMPO_FOTOGRAMA + aleatorio.nextInt(VARIACION_TIEMPO);
    }

    public int getDuracion() {
        return duracion;
    }
    
    @Override
    public String toString() {
        return "Fotograma(" + id + ", dur: " + duracion + ")";
    }
}
