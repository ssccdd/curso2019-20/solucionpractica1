/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uja.ssccdd.curso1920.solucionpractica1;

/**
 *
 * @author fconde
 */
public class Finalizador implements Runnable {

    @Override
    public void run() {
        System.out.println("\n-- FINALIZADOR: Todos los generadores han terminado");
        System.out.println("                Notificando a los renderizadores para que terminen\n");
        RenderizadorEscenas.setTerminadosGeneradores(true);
    }
}
