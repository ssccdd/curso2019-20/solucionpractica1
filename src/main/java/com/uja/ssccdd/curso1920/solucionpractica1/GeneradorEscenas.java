/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uja.ssccdd.curso1920.solucionpractica1;

import static com.uja.ssccdd.curso1920.solucionpractica1.Constantes.MIN_NUM_ESCENAS;
import static com.uja.ssccdd.curso1920.solucionpractica1.Constantes.MIN_TIEMPO_GENERACION;
import com.uja.ssccdd.curso1920.solucionpractica1.Constantes.Prioridad;
import static com.uja.ssccdd.curso1920.solucionpractica1.Constantes.VARIACION_NUM_ESCENAS;
import static com.uja.ssccdd.curso1920.solucionpractica1.Constantes.VARIACION_TIEMPO;
import static com.uja.ssccdd.curso1920.solucionpractica1.Constantes.aleatorio;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.Callable;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fconde
 */
public class GeneradorEscenas implements Callable<List<Escena>> {
    private String id;
    
    private ArrayList<Escena> listaAltaPrioridad;
    private Semaphore emptySemAltaPrioridad;
    private Semaphore fillSemAltaPrioridad;
    private ReentrantLock mutexAltaPrioridad;
    private ArrayList<Escena> listaBajaPrioridad;
    private Semaphore emptySemBajaPrioridad;
    private Semaphore fillSemBajaPrioridad;
    private ReentrantLock mutexBajaPrioridad;
    
    private CyclicBarrier barrier;

    public GeneradorEscenas(String id, ArrayList<Escena> listaAltaPrioridad, 
            Semaphore emptySemAltaPrioridad, Semaphore fillSemAltaPrioridad, 
            ReentrantLock mutexAltaPrioridad, ArrayList<Escena> listaBajaPrioridad, 
            Semaphore emptySemBajaPrioridad, Semaphore fillSemBajaPrioridad, 
            ReentrantLock mutexBajaPrioridad, CyclicBarrier barrier) {
        
        this.id = id;
        this.listaAltaPrioridad = listaAltaPrioridad;
        this.emptySemAltaPrioridad = emptySemAltaPrioridad;
        this.fillSemAltaPrioridad = fillSemAltaPrioridad;
        this.mutexAltaPrioridad = mutexAltaPrioridad;
        this.listaBajaPrioridad = listaBajaPrioridad;
        this.emptySemBajaPrioridad = emptySemBajaPrioridad;
        this.fillSemBajaPrioridad = fillSemBajaPrioridad;
        this.mutexBajaPrioridad = mutexBajaPrioridad;
        this.barrier = barrier;
    }
    
    @Override
    public List<Escena> call() {
        List<Escena> listaEscenasGeneradas = new ArrayList<Escena>();
        int numEscenasAGenerar = MIN_NUM_ESCENAS + aleatorio.nextInt(VARIACION_NUM_ESCENAS);
        System.out.println("   " + id + ": Ha iniciado la ejecucion" +
                "   Generando: " + numEscenasAGenerar + " escenas\n");
        for (int i=0; i<numEscenasAGenerar; i++) {
            int tiempoGeneracionEscena = MIN_TIEMPO_GENERACION + aleatorio.nextInt(VARIACION_TIEMPO);
            try {
                TimeUnit.SECONDS.sleep(tiempoGeneracionEscena);
            } catch (InterruptedException ex) {
                Logger.getLogger(GeneradorEscenas.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            Escena escena = new Escena();
            escena.inicializarEscena();
            listaEscenasGeneradas.add(escena);
            
            System.out.println("   " + id + ": Generando escena " + escena.getId() + 
                    "," + escena.getPrioridad() + ", " + escena.getHoraGeneracionStr());
            
            if (escena.getPrioridad() == Prioridad.ALTA) {
                try {
                    fillSemAltaPrioridad.acquire();
                    mutexAltaPrioridad.lock();
                    listaAltaPrioridad.add(escena);
                    mutexAltaPrioridad.unlock();
                    emptySemAltaPrioridad.release();
                } catch (InterruptedException ex) {
                    Logger.getLogger(GeneradorEscenas.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                try {
                    fillSemBajaPrioridad.acquire();
                    mutexBajaPrioridad.lock();
                    listaBajaPrioridad.add(escena);
                    mutexBajaPrioridad.unlock();
                    emptySemBajaPrioridad.release();
                } catch (InterruptedException ex) {
                    Logger.getLogger(GeneradorEscenas.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
        try {
            barrier.await();
        } catch (InterruptedException ex) {
            Logger.getLogger(GeneradorEscenas.class.getName()).log(Level.SEVERE, null, ex);
        } catch (BrokenBarrierException ex) {
            Logger.getLogger(GeneradorEscenas.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return listaEscenasGeneradas;
    }
}
